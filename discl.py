################################################################
# File: discl.py
# Title: Discord Client Library
# Author: Sorch <sorch@protonmail.ch>
# Version: 0.2a
# Description:
#  An event-based library for connecting to multiple discord rooms
###################################################################

##########
# License
##########################################################################
# Copyright 2016 Contributing Authors
# This program is distributed under the terms of the GNU GPL VERSION 2.
##########################################################################

# MISCS
###
BASE          = 'https://discordapp.com'
API_BASE      = BASE     + '/api/v7'
GATEWAY       = API_BASE + '/gateway'
USERS         = API_BASE + '/users'
REGISTER      = API_BASE + '/auth/register'
LOGIN         = API_BASE + '/auth/login'
LOGOUT        = API_BASE + '/auth/logout'
SERVERS       = API_BASE + '/guilds'
CHANNELS      = API_BASE + '/channels'
AT_ME         = API_BASE + '/users/@me'
BOT_GATEWAY   =	GATEWAY + '/bot'

#IMPORTS
###

import threading
from ws4py.client import WebSocketBaseClient
import requests
import json, re, time, copy, json, itertools, socket
import sys
import time
import zlib



try:
	import queue
except ImportError:
	import Queue as queue

class Error(Exception):
	pass

class GroupClient(WebSocketBaseClient):
	def setup(self, mgr):
		self._mgr = mgr
		return self

	def closed(self, *args):
		pass

	def opened(self):
		pass

	def handshake_ok(self):
		pass

	def received_message(self, msg):
		msg = msg
		#print("hello")
		if msg.is_binary:
			msg = zlib.decompress(msg.data, 15, 10490000)
			msg = msg.decode("utf-8")
		else:
			msg = msg.data.decode('utf-8')
		self._mgr._recvQueue.put_nowait(msg)

class Message:
	def __init__(self, **kwargs):
		for kw in kwargs:
			setattr(self, kw, kwargs[kw])

class User:
	def __init__(self, **kwargs):
		for kw in kwargs:
			setattr(self, kw, kwargs[kw])

	def can(self, permission):
		return permission in self.permissions

class Channel:
	def __init__(self, **kwargs):
		for kw in kwargs:
			setattr(self, kw, kwargs[kw])

class Role(object):
	def __init__(self, **kwargs):
		self.update(**kwargs)

	def update(self, **kwargs):
		self.id = kwargs.get('id')
		self.name = kwargs.get('name')
		self.permissions = Permissions(kwargs.get('permissions', 0))

def create_permission_masks(cls):
	cls.NONE = cls(0)
	cls.ALL = cls(0b00000011111100111111110000111111)
	cls.ALL_CHANNEL = cls(0b00000011111100111111110000011001)
	cls.GENERAL = cls(0b00000000000000000000000000111111)
	cls.TEXT = cls(0b00000000000000111111110000000000)
	cls.VOICE = cls(0b00000011111100000000000000000000)
	return cls

@create_permission_masks
class Permissions(object):
	_bits = {
		'ban_members': 1,
		'kick_members': 2,
		'read_messages': 10,
		'manage_messages': 13,
		'instant_invite' : 0,
		'manage_roles' : 3,
		'manage_channels': 4,
		'manage_server': 5,
		'send_messages': 11,
		'send_tts_messages': 12,
		'embed_links': 14,
		'read_message_history': 16,
		'mention_everyone': 17,
		'connect': 20,
		'speak': 21,
		'mute_members': 22,
		'deafen_members': 23,
		'move_members': 24,
		'use_voice_activation': 25
	}

	def __init__(self, permissions = 0, **kwargs):
		self.value = permissions

	def _bit(self, index):
		return bool((self.value >> index) & 1)

	def _set(self, index, value):
		if value:
			self.value |= (1 << index)
		else:
			self.value &= ~(1 << index)

	def can(self, permission):
		if permission in self._bits: return self._bit(self._bits[permission])

	def get(self):
		return [k for k in self._bits if self.can(k)]


class Server(object):
	def __init__(self, **kwargs):
		self._from_data(kwargs)

	def _from_data(self, guild):
		############
		
		self.users  = []
		self.name = guild["name"]
		self.region = guild.get('region')
		self.afk_timeout = guild.get('afk_timeout')
		self.icon = guild.get('icon')
		self.unavailable = guild.get('unavailable', False)
		self.id = guild['id']
		self.channels = [Channel(server = self, **c) for c in guild['channels']]
		self.roles = [Role(everyone=(self.id == r['id']), **r) for r in guild['roles']]
		for data in guild['members']:
			user = User(
				joined_at = data["joined_at"],
				mute = data["mute"],
				deaf = data["deaf"],
				discrim = data["user"]["discriminator"],
				name = data["user"]["username"],
				avatar = data["user"]["avatar"],
				id = data["user"]["id"],
				mention = '<@{0}>'.format(data["user"]["id"]),
				roles = [x for x in self.roles if x.id in data['roles']],
				permissions = list(set(sum([x.permissions.get() for x in self.roles if x.id in data['roles']], []))),
			)
			if user.id == guild['owner_id']: self.owner = user
			self.users.append(user)

class DiscordClient(object):
	def __init__(self, token = None):
		if token == None:
			raise Error("No token please add your bot token")
			
		self._token = token
		self._headers = dict(Authorization = "Bot " + self._token if self._token else "")
		self._ready = False
		self._run = True
		self._recvQueue = queue.Queue()
		self._servers = []

	def _isConnected(self):
		#if not self._sock or not self._sock.client_terminated or not self._sock.server_terminated: return False
		#if self._sock.terminated: return False
		return True


	def _ping(self):
		while self._run and self._isConnected():
			time.sleep(20)
			payload = { 'op': 1, 'd': int(time.time())}
			try:
				self._sock.send(json.dumps(payload, separators=(',', ':')))
			except socket.error:
				self.stop()
				break
			except Exception as e:
				self._callEvent('onError', e)

	def _recv(self):
		while self._run and self._isConnected():
			try:
				if not self._sock.once():
					self.stop()
			except Exception as e:
				self._callEvent('onError', e)

	def _getData(self):
		try:
			return self._recvQueue.get()
		except queue.Empty:
			pass
		except Exception as e:
			self._callEvent('onError', e)

	def _main(self):
		if self._login():
			while self._run and self._isConnected():
				data = self._getData()
				if data:
					try:
						parsed = json.loads(data)
						if parsed["t"] != None:
							self._route(parsed["t"], parsed["d"])
					except Exception as e:
						self._callEvent('onError', e)
			self._callEvent('onDisConnected')

	def _add_server(self, guild):
		server = Server(**guild)
		self._servers.append(server)
		return server

	def _remove_server(self, guildid):
		self._servers = [g for g in self._servers if g.id != guildid]
		return True

	def _parseMention(self, ctx):
		return re.findall(r'<@(\d+)>', ctx)

	def _login(self):
		sentobject = requests.get(AT_ME, headers = self._headers)
		if sentobject.status_code == 400:
			self._callEvent('onBadLogin')
			return False
		body = sentobject.json()
		self._headers['Authorization'] = "Bot " + self._token if self._token else ""
		gateway = requests.get(BOT_GATEWAY, headers = self._headers)
		self._sock = GroupClient("%s" % gateway.json().get('url')).setup(self)
		self._sock.connect()
		self._sendFirstLogin()
		self._pingTimer = threading.Thread(target = self._ping)
		self._pingTimer.daemon = False
		self._pingTimer.start()
		self._recvTimer = threading.Thread(target = self._recv)
		self._recvTimer.daemon = False
		self._recvTimer.start()
		return True

	def _sendFirstLogin(self):
		second_payload = { 'op': 2, 'd': { 'token': self._token, 'compress': True, 'properties': {'$os': sys.platform, '$browser': 'discl.py', '$device': 'discl.py', '$referrer': '', '$referring_domain': '' }, 'v': 3 } }
		self._sock.send(json.dumps(second_payload, separators=(',', ':')))

	def _route(self, cmd, args):
		if hasattr(self, "_r_%s" % (cmd.lower())):
			getattr(self, "_r_%s" % (cmd.lower()))(args)

	def _callEvent(self, evt, *args):
		if hasattr(self, evt):
			return getattr(self, evt)(*args)

	def getServer(self, i):
		if i is None: return None
		for server in self._servers:
			if i == server.id:
				return server

	def getChannel(self, i):
		for serv in self._servers:
			for ch in serv.channels:
				if ch.id == i:
					return ch

	def getMember(self, si, ai):
		for serv in self._servers:
			if si == serv.id:
				for ah in serv.users:
					if ah.id == ai: return ah

	def _inviteParse(self, invite):
		rx = r'(?:https?\:\/\/)?discord\.gg\/(.+)'
		m = re.match(rx, invite)
		if m:
			return m.group(1)

	def getInvite(self, inv):
		dest = self._inviteParse(inv)
		url = '{0}/invite/{1}'.format(API_BASE, dest)
		return requests.get(url, headers=self._headers).json()

	def joinServer(self, inv):
		dest = self._inviteParse(inv)
		url = '{0}/invite/{1}'.format(API_BASE, dest)
		return requests.post(url, headers=self._headers)

	def leaveServer(self, i):
		url = '{0}/{1}'.format(SERVERS, i)
		return requests.delete(url, headers=self._headers)

	def deleteMessage(self, i, u):
		url = '{}/{}/messages/{}'.format(CHANNELS, i,u)
		return requests.delete(url, headers=self._headers)

	def sendMessage(self, dest, msg, embed = None):
		payload = {}
		if msg:
			payload['content'] = msg	
		if embed:
			payload['embed'] = embed
		url = '{base}/{id}/messages'.format(base = CHANNELS, id = dest)
		return requests.post(url, json = payload, headers = self._headers)

	def stop(self):
		self._run = False
		self._sock.close()
		return True

	def _r_ready(self, args):
		guilds = args.get('guilds')
		for guild in guilds:
			self._add_server(guild)
		self._ready = True
		self._callEvent('onConnected')

	def _r_presence_update(self, args):
		self._callEvent("OnPresenceUpdate", args["game"])


	def _r_guild_create(self, args):
		self._callEvent('onServerAdded', self._add_server(args))

	def _r_guild_delete(self, args):
		self._remove_server(args['id'])
		self._callEvent('onServerRemoved', args['id'])
		
	def _r_message_update(self, args):
		if not self.getChannel(args["channel_id"]): return
		msg = Message(
			id = args["id"],
			timestamp = args["timestamp"],
			channel = self.getChannel(args["channel_id"]),
			server = self.getChannel(args["channel_id"]).server,
			user = self.getMember(self.getChannel(args["channel_id"]).server.id, args["author"]["id"]),
			body = args["content"],
			nonce = args["nonce"],
		)
		self._callEvent('onMessageEdit', msg)
		
		
		
	def _r_message_delete(self, args):
		mid = args["id"]
		channel_id = args["channel_id"]
		guild_id = args["guild_id"]
		

	def _r_message_create(self, args):
		if not self.getChannel(args["channel_id"]): return
		msg = Message(
			id = args["id"],
			timestamp = args["timestamp"],
			channel = self.getChannel(args["channel_id"]),
			server = self.getChannel(args["channel_id"]).server,
			user = self.getMember(self.getChannel(args["channel_id"]).server.id, args["author"]["id"]),
			body = args["content"],
			nonce = args["nonce"],
		)
		self._callEvent('onMessage', msg)

